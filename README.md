# Atlas CLI Typescript refapp plugin

https://developer.atlassian.com/platform/micros2/gettingstarted/atlas-cli/

https://stash.atlassian.com/projects/MICROS/repos/atlas-cli/browse/PLUGINS.md

## Build a plugin executable for OSX and Linux

1. Update `version` field in your package.json
2. `npm run build`

This will create two archives with your plugin executables for Linux and OSX in your `dist` directory.

## Test your plugin

1. Install [Atlas CLI](https://developer.atlassian.com/platform/micros2/gettingstarted/atlas-cli/)
2. Install some of the plugins: `atlas plugin install -t micros2`
3. Add your plugin to `~/.config/atlassian/atlas/plugins-installed.toml` file so it looks like this:

```toml
[[plugin]]
  name = "deployments"
  summary = "Micros2 Deployment Management"
  version = "0.0-211-g86b6e20"
  sha256 = ""
  exec = "/path/to/atlassian/atlas/plugin/deployments"

[[plugin]]
  name = "myplugin"
  summary = "My awesome plugin"
  version = "0.0.1"
  sha256 = ""
  exec = "/path/to/plugin/platform/executable"
```

Now you can run your plugin with `atlas myplugin --help`. Try running `atlas myplugin --debug` for example.

### How do I write logs

You should write logs to the file decriptor which Atlas CLI defines in a `ATLAS_LOG_OUTPUT` environment variable. An example implementation is in "src/log.ts" file. When your plugin writes logs to this file descriptor Atlas CLI sends the log to the Atlas CLI service which writes it to [Splunk](https://splunk.paas-inf.net/en-US/app/search/search?q=search%20eventtype%3Dmicros_atlas-cli-logs%20m.t%3Dapplication&display.page.search.mode=smart&dispatch.sample_ratio=1&earliest=-15m&latest=now&display.events.fields=%5B%22log%22%2C%22%40timestamp%22%2C%22message%22%2C%22msg%22%2C%22topic%22%2C%22user%22%2C%22cfEvent.ResourceType%22%2C%22cfEvent.ResourceStatus%22%2C%22cfEvent.ResourceStatusReason%22%2C%22cli.msg%22%2C%22cli.user%22%5D&sid=1544062660.330_C86D7B90-C052-42B9-907B-4ABD7D997D72). Make sure to check "last 15 minutes" in Splunk to see your logs. As you can see from the example you need to send a valid JSON string followed by a new line. Atlas CLI service puts your JSON string into a "cli." field and adds a couple of its own fields.

![splunk example](./docs/splunk.png)

### I can't find my logs in Splunk

Run Atlas CLI with `ATLAS_LOG_LEVEL=debug` environment variable value, so it will show what's wrong.

`ATLAS_LOG_LEVEL=debug atlas refapp --log`

## Host your executable

An easy way to do that is to use [Statlas](https://hello.atlassian.net/wiki/spaces/DEV/blog/2015/10/08/171776088/Do+you+have+any+static+content+to+host+-+use+statlas.atlassian.io). What you need to remember about Statlas:

* It's a place to host the static files. Atlas CLI and others use it to distribute their executables
* It's Atlassian-internal, there will be no access to it from Bitbucket Pipelines (this may change later)
* There's some sync problem so sometimes your newly uploaded files can be missing.

### Upload your manifest

First things first: upload your manifest.toml file to Statlas. Choose secret that you can use later.

```bash
$ curl -X PUT -H 'authorization: Token <SECRET>' -T manifest.toml https://statlas.prod.atl-paas.net/dsorin-atlas-cli-plugin-refapp/manifest.toml
```

### Build your plugin

Build your plugin executable (see the example above).

### Add `[[release]]` section to your manifest

Add the newly built version of your plugin to the manifest.toml file. This should be something like this:

```toml
owners = [ "dsorin",]
description = "Atlas CLI refapp plugin"
tags = [ "refapp", "ecosystem",]
summary = "Atlas CLI refapp"
name = "resource"

[[release]]
channels = [ "stable",]
timestamp = "2018-11-13T18:23:23.618101Z"
variants = [ "darwin-amd64", "linux-amd64",]
version = "1.2.3"

[[release]]
channels = [ "stable",]
timestamp = "2018-11-11T18:23:23.618101Z"
variants = [ "darwin-amd64", "linux-amd64",]
version = "1.2.2"
```

Manifest.toml file should contain information about all published versions of the plugin. 

### Upload your newly built plugin and changed manifest

```bash
# manifest
$ curl -X PUT -H 'authorization: Token <SECRET>' -T manifest.toml https://statlas.prod.atl-paas.net/dsorin-atlas-cli-plugin-refapp/manifest.toml

# osx executable in an archive
$ curl -X PUT -H 'authorization: Token <SECRET>' -T dist/darwin-amd64.tar.gz https://statlas.prod.atl-paas.net/dsorin-atlas-cli-plugin-refapp/releases/<VERSION>/darwin-amd64.tar.gz

# linux executable in an archive
$ curl -X PUT -H 'authorization: Token <SECRET>' -T dist/linux-amd64.tar.gz https://statlas.prod.atl-paas.net/dsorin-atlas-cli-plugin-refapp/releases/<VERSION>/linux-amd64.tar.gz
```
